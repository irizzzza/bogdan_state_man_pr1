import 'package:bogdan_state_management_pr_1/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_super_state/flutter_super_state.dart';

class ColorModule extends StoreModule {

  Color color;

  ColorModule(Store store): super(store);

  void changeColor(int colorIndex) {
    print(color);
    setState(() {
        color = appColors[colorIndex];
    });
  }
}