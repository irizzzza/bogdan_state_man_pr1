import 'package:bogdan_state_management_pr_1/color_module.dart';
import 'package:bogdan_state_management_pr_1/pages/counter_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_super_state/flutter_super_state.dart';

void main() {
  final store = Store();
  ColorModule(store);

  runApp(
    StoreProvider(
      store: store,
      child: MyApp(),
    ),
  );
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CounterScreen(),
    );
  }
}
