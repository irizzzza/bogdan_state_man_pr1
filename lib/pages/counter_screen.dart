import 'package:bogdan_state_management_pr_1/color_module.dart';
import 'package:bogdan_state_management_pr_1/res/consts.dart';
import 'package:bogdan_state_management_pr_1/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_super_state/flutter_super_state.dart';

class CounterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ModuleBuilder<ColorModule>(
      builder: (context, colorModule) {
        return Scaffold(
          backgroundColor: colorModule.color,
          appBar: AppBar(
            title: Text(TITLE),
          ),
          drawer: AppDrawer(),
          body: Center(
            child: Text(TEXT),
          ),
        );
      },
    );
  }
}
