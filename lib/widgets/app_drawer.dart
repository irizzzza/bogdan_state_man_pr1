import 'package:bogdan_state_management_pr_1/color_module.dart';
import 'package:bogdan_state_management_pr_1/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_super_state/flutter_super_state.dart';



class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ModuleBuilder<ColorModule>(
        builder: (context, colorModule) {
          return ListView.builder(
            itemCount: appColors.length,
            itemBuilder: (ctx, index) {
              return RaisedButton(
                onPressed: () {
                  colorModule.changeColor(index);
                  Navigator.of(context).pop();
                },
                color: appColors[index],
              );
            },
          );
        },
      ),
    );
  }
}
